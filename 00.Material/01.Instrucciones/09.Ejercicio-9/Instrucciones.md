# Ejercicio 9
Comandos que utilizaremos: [Manual](Manual.md)

## Usemos cherry-pick

* Volvamos a la rama master si no nos encontramos allí:

```
git checkout master
```

* Creemos 2 ramas

```
git checkout -b cherry1
git checkout master
git checkout -b cherry2
```

* En la rama cherry 2 añadiremos una clase dummy con el nombre Dummy.java y la colamos en el mismo directorio que la clase SayayinDTO

```java
public class Dummy {
  // this is a dummy class
}
```

* Realizamos add y commit de esta nueva clase

```
git add .
git commit -m "Cherry test"
```

* Ahora entremos a la clase SayayinDTO y cambiemos cualquier cosa de la misma.
* Hagamos add y commit de estos cambios también

```
git add .
git commit -m "Cherry mod"
```

* Antes de irnos con el comando git log sacaremos el hash del primer commit que realizamos "cherry test", lo guardamos en un lugar cualquier ya que luego lo necesitaremos

```
git log
```

* Nos pasamos a la rama Cherry1

```
git checkout cherry1
```

* Supongamos que este desarrollador necesita la clase Dummy.java para continuar su trabajo, sin embargo, no desea traerse los cambios de la clase SayayinDTO ya que estos siguen inestable. Hacemos uso de cherry-pick para que, con el hash del commit nos traigamos a nuestra rama cherry1 exclusivamente los cambios de este commit e ignoremos el del segundo

```
git cherry-pick #HASH
```

Veremos la clase dummy aparecer sin embargo no tendremos los demás cambios de esta rama.